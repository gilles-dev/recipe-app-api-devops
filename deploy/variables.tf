variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-app-api-devops"

}

variable "contact" {
  default = "gilles.guglielmoni@gmail.com"
}

variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

variable "bastion_key_name" {
  default = "gilles@gitlab"
}

variable "ecr_image_api" {
  description = "ECR image for API"
  default     = "727581762290.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-devops:latest"
}

variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "727581762290.dkr.ecr.us-east-1.amazonaws.com/recipe-app-api-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
}

variable "dns_zone_name" {
  description = "Domain name"
  default     = "guglielmoni.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    dev        = "api.dev"
    staging    = "api.staging"
    production = "api"
  }
}