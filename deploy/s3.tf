resource "aws_s3_bucket" "app_public_files" {
  bucket        = "ggu-${local.prefix}-files"
  acl           = "public-read"
  force_destroy = true
}

